#include <stdio.h>
#include <ctype.h>
#include <math.h>
#define MAXLINE 1000

int MyGetLine(char line[], int maxLine)
{
    int i, c;

    i = 0;
    while (--maxLine && (c = getchar()) != EOF && c != '\n')
    {
        line[i++] = c;
    }

    if (c == '\n')
    {
        line[i++] = c;
    }

    line[i++] = '\0';
    return i;
}
int StrIndex(const char *line, const char *pattern)
{
    int i, j, k;
    for (i = 0; line[i] != '\0'; ++i)
    {
        for (j = i, k = 0; pattern[k] != '\0' && line[j] == pattern[k]; ++j, ++k)
        {
            ;
        }
        if (k > 0 && pattern[k] == '\0')
        {
            return i;
        }
    }
    return -1;
}

int StrIndex_2(const char *line, const char *pattern)
{
    int i, j, k;
    int maxL = 0;
    for (i = 0; line[i] != '\0'; ++i)
    {
        for (j = i, k = 0; pattern[k] != '\0' && pattern[k] == line[j]; ++j, ++k)
        {
            ;
        }
        if (k > 0 && pattern[k] == '\0')
        {
            if (i > maxL)
            {
                maxL = i;
            }
        }
    }

    if (maxL > 0)
    {
        return maxL;
    }
    return -1;
}

// 将字符串转化为双浮点类数
double atof(const char *s)
{
    double val, power;
    int sign, i, exp_sign, exponent;
    for (i = 0; isspace(s[i]); ++i)
    {
        ;
    }

    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-')
    {
        i++;
    }

    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if (s[i] == '.')
        i++;
    for (power = 1.0; isdigit(s[i]); i++)
    {
        val = 10.0 * val + (s[i] - '0');//实际的值
        power *= 10;
    }
    if(s[i] == 'e' || s[i] == 'E'){
        i++;
        exp_sign = (s[i++] == '-') ? -1 : 1;
        for(exponent=0; isdigit(s[i]); ++i){
            exponent = 10 * exponent + s[i] - '0';//10的指数
        }
        val = val * pow(10, exp_sign * exponent);//此时val的乘以e之后的数
    }
    
    return sign * val / power;
}
int main()
{
    char line[MAXLINE];
    const char pattern[] = "ould";
    while (MyGetLine(line, MAXLINE) > 0)
    {
        //double d = atof(line);
        //printf("%f\n", d);
        printf("%f\n", atof("123.45e-6"));  // 输出：0.000123450
        printf("%f\n", atof("123.45E+6"));  // 输出：123450000.000000
        printf("%f\n", atof("-123.45e2"));  // 输出：-12345.000000
        printf("%f\n", atof("1.23E-4"));    // 输出：0.000123
    }

    return 0;
}
