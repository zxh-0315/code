#include <stdio.h>
#include <string.h>

void reverse(char s[]){
    for(int i=0, j=strlen(s)-1; i<j; i++,j--){
        char temp=s[i];
        s[i]=s[j];
        s[j]=temp;
    }
}
void itoa(int n, char s[], int b){
    int i=0;
    unsigned int sign = (n < 0)? -n:n;
    
    do{
        s[i++]=sign%10 + '0';
    }while((sign /= 10) > 0);
    
    while(i < b-1){
        s[i++]='0';
    }
    if(n < 0){
        s[i++]='-';
    }else{
        s[i++]='0';
    }
    
    s[i]='\0';
    reverse(s);
}
void itob(int n, char s[], int b){
    int i=0;
    unsigned int sign=(n < 0)? -n:n;
    do{
        char a = sign % b + '0';
        if(a > '9'){
            a = a - '9' + 'A' - 1;
        }
        s[i++] = a;
    }while((sign /=b) > 0);
    if(n < 0){
        s[i++]='-';
    }
    s[i]='\0';
    reverse(s);
}
int main(){
    int n=255;
    char s[100];
    itoa(n, s, 8);
    printf("123:%s\n", s);
}
